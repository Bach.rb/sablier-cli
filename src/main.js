import Vue from 'vue'
import VueRouter from 'vue-router'
import routes from './routes.js'
import App from './App.vue'
// Plugins 
import ApiPlugin from "./services/ApiPlugin"
import AuthPlugin from "./services/AuthPlugin"
import StopwatchPlugin from "./services/StopwatchPlugin"
import DateTimeHelpers from "./services/DateTimeHelpersPlugin"
var VueCookie = require('vue-cookie');

Vue.use(VueRouter)
Vue.use(VueCookie)
Vue.use(AuthPlugin)
Vue.use(ApiPlugin)
Vue.use(StopwatchPlugin)
Vue.use(DateTimeHelpers)


const router = new VueRouter({
  routes
})

new Vue({
  router, 
  render: h => h(App)
}).$mount('#app')