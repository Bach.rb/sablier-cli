import ProjectView from './components/ProjectView.vue';
import LoginView from './components/LoginView.vue';
import Subscribe from './components/SubscribeView.vue';
import Landing from './components/LandingView.vue';
import Onboarding from './components/Onboarding.vue';
import test from './components/widgets/Stopwatch.vue'

const routes = [
    { path: '/p/:projectId', name:"dashboard", component: ProjectView },
    { path: '/Login', name:"login", component: LoginView }, 
    { path: '/Subscribe', name:"subscribe", component: Subscribe },
    { path: '/', name:"home", component: Landing }, 
    { path: '/onboarding', name:"onboarding", component: Onboarding },
    { path: '/test', name:"test", component: test }
];

export default routes;