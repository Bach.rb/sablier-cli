import moment from 'moment'

// Classe minuteur
class Stopwatch {
    constructor() {
        this.isRunning = false;
        this.startingDate = null;
        this.endingDate = null;
        this.pausedDate = null;
        this.timeinPause = 0;
    }


    startTimer() {
        if (!this.startingDate)
            this.startingDate = new moment();
        else {
            this.timeinPause += new moment().diff(this.pausedDate, 'seconds')
            this.pausedDate = null;
        }

        this.isRunning = true;
    }

    pauseTimer() {
        if (this.isRunning) {
            this.pausedDate = new moment();
            this.isRunning = false;
        }
    }

    stopTimer() {
        this.endingDate = new moment();
        this.isRunning = false;

    }

    resetTimer() {
        this.isRunning = false;
        this.startingDate = null;
        this.endingDate = null;
        this.pausedDate = null;
        this.timeinPause = 0;
    }

    outputStopwatch() {
        // Not running
        if (!this.startingDate) return null;

        // Ended
        if (this.endingDate) {
            var realEndingDate = this.endingDate.clone().subtract(this.timeinPause, 'seconds');
            return {
                days: realEndingDate.diff(this.startingDate, 'days'),
                hours: realEndingDate.diff(this.startingDate, 'hours'),
                minutes: realEndingDate.diff(this.startingDate, 'minutes'),
                seconds: realEndingDate.diff(this.startingDate, 'seconds')
            }
        }

        // Running
        if (!this.pausedDate) {
            var realActualDate = new moment().subtract(this.timeinPause, 'seconds');
            return {
                days: realActualDate.diff(this.startingDate, 'days'),
                hours: realActualDate.diff(this.startingDate, 'hours'),
                minutes: realActualDate.diff(this.startingDate, 'minutes'),
                seconds: realActualDate.diff(this.startingDate, 'seconds')
            }
        }
        else {
            var realPausedDate = this.pausedDate.clone().subtract(this.timeinPause, 'seconds');
            return {
                days: realPausedDate.diff(this.startingDate, 'days'),
                hours: realPausedDate.diff(this.startingDate, 'hours'),
                minutes: realPausedDate.diff(this.startingDate, 'minutes'),
                seconds: realPausedDate.diff(this.startingDate, 'seconds')
            }
        }
    }

}


export default {
    install: function (Vue) {
        Vue.prototype.$stpw = new Stopwatch();
    }
}