//import jwt from "jsonwebtoken";

var functions = {
    isLoggedIn: function (cookie) {
        if (cookie.get(process.env.VUE_APP_COOKIE_NAME)) return true;
        else return false;
    }, 
    logOut: function(cookie) {
        if (cookie.get(process.env.VUE_APP_COOKIE_NAME)) cookie.delete(process.env.VUE_APP_COOKIE_NAME);
    }
}

export default {
    install: function (Vue) {
        Vue.prototype.$auth = functions;
    }
}