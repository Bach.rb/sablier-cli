import axios from "axios";
axios.defaults.withCredentials = true;

var functions = {
  login: function (username, password) {
    return axios
      .post(process.env.VUE_APP_API_URL + "/signin", {
        username: username,
        passwd: password
      });
  },
  subscribe: function (email, password) {
    return axios.post(process.env.VUE_APP_API_URL + "/subscribe", {
      email: email,
      passwd: password
    });
  }, 
  getTimesheets: function(projectId) {
    return axios.get(process.env.VUE_APP_API_URL + "/resources/project/" + projectId + "/timesheet");
  }, 
  getProject: function(projectId) {
    return axios.get(process.env.VUE_APP_API_URL + "/resources/project/" + projectId);
  }, 
  saveTimesheet: function(projectId, timesheet) {
    return axios.put(process.env.VUE_APP_API_URL + "/resources/project/" + projectId + "/timesheet", {
      data: timesheet
    }); 
  }

}

export default {
  install: function (Vue) {
    Vue.prototype.$caller = functions;
  }
}