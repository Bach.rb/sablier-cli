import moment from 'moment'

var functions = {
    datetoHumanReadableFormat: function (date) {
        return moment(date).fromNow();
    }, 
    durationtoHumanReadableFormat: function (seconds) {
        var dur = moment.duration(seconds, 'seconds'); 
        var strDuration = ""; 
        
        strDuration += dur.days() > 0 ? dur.days() + "d" : ""; 
        strDuration += dur.hours() > 0 ? dur.hours() + "h" : ""; 
        strDuration += dur.minutes() > 0 ? dur.minutes() + "m" : ""; 
        strDuration += dur.seconds() > 0 ? dur.seconds() + "s" : ""; 

        return strDuration;
    }
}

export default {
    install: function (Vue) {
        Vue.prototype.$datetimeHelpers = functions;
    }
}